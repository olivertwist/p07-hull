//
//  GameScene.swift
//  project7
//
//  Created by oliver on 4/22/17.
//  Copyright © 2017 oliver. All rights reserved.
//

import SpriteKit
import GameplayKit
import AVFoundation

let rickCategory : UInt32 = 0x1 << 0;
let enemyCategory : UInt32 = 0x1 << 1;
let flaskCategory : UInt32 = 0x1 << 3;
let crystalCategory : UInt32 = 0x1 << 4;
let floorCategory : UInt32 = 0x1 << 5;

let portalCategory : UInt32 = 0x1 << 6;


class GameScene: SKScene {
	
	private var portal: SKSpriteNode?
	private var rick: SKSpriteNode?
	private var morty: SKSpriteNode?
	private var cam:SKCameraNode!
	private var monster:SKSpriteNode?
	private var floor:SKSpriteNode?
	private var audioPlayer:AVAudioPlayer?
	private var rickFrames:[SKTexture]!
	private var gunFrames:[SKTexture]!
	private var mortyFrames:[SKTexture]!
	private var enemyInView:Bool?
	private var cutSceneOver:Bool!
	private var enemy:SKSpriteNode?
	
    override func didMove(to view: SKView) {
		self.setUpTextures()
		self.cutScene()
	}
	func openPortal(){
		
		portal = SKSpriteNode(color: SKColor.clear, size: CGSize(width: 24, height: 23))
		portal?.position = CGPoint(x: 0, y: 0)
		var blinkFrames = [SKTexture]()
		
		
		let portalAnimationFrames = SKTextureAtlas(named: "portal")
		let numImages = portalAnimationFrames.textureNames.count
		for i in 0...numImages-1 {
			var portalName = "portal0\(i).png"
			if i < 10 {
				portalName = "portal0\(i).png"
			}
			else{
				portalName = "portal\(i).png"
			}
			blinkFrames.append(portalAnimationFrames.textureNamed(portalName))
		}
		
		portal?.run(SKAction.repeatForever(
			SKAction.animate(with: blinkFrames,
			                 timePerFrame: 0.05,
			                 resize: false,
			                 restore: true)),
		           withKey:"portalSpin")
		
		portal?.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),SKAction.scale(by: 10, duration: 1),SKAction.wait(forDuration: 4),SKAction.scale(by: 0.1, duration: 1), SKAction.removeFromParent()]))
		self.addChild(portal!)
		
		portal?.run(SKAction.sequence([SKAction.wait(forDuration: 0.75), SKAction.playSoundFileNamed("rickportal.wav", waitForCompletion: false)]))
		

	}
	
    func touchDown(atPoint pos : CGPoint) {

	}
    
    func touchMoved(toPoint pos : CGPoint) {
  
	}
    
    func touchUp(atPoint pos : CGPoint) {

	}
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		if(cutSceneOver)!{
			shoot()
			
			
			
			
			
			
		}
	}
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchMoved(toPoint: t.location(in: self)) }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
	
	

	
	func cutScene(){
		cutSceneOver = false;
		cam = SKCameraNode()
		self.camera = cam
		self.addChild(cam)
		
		cam.position = CGPoint(x: 0, y: 0)
		openPortal()
		
		floor = self.childNode(withName: "floor") as? SKSpriteNode
		
		
		let rick_texture = SKTexture(imageNamed: "rick_start")
		
		
		rick = SKSpriteNode(color: SKColor.clear, size: CGSize(width: 25, height: 32))
		rick?.zPosition = 1
		rick?.position = (portal?.position)!
		self.addChild(rick!)
		
		let morty_texture = SKTexture(imageNamed: "morty_start")
		morty = SKSpriteNode(color: SKColor.clear, size: CGSize(width: 16, height: 22))
		
		morty?.position = (portal?.position)!
		self.addChild(morty!)
		
		
		
		
		morty?.run(SKAction.sequence([SKAction.wait(forDuration: 2),SKAction.setTexture(morty_texture), SKAction.scale(by: 3, duration: 1)]))
		
		
		rick?.run(SKAction.sequence([SKAction.wait(forDuration: 2),SKAction.setTexture(rick_texture), SKAction.scale(by: 3, duration: 1)]))
		
		
		
		morty?.zPosition = 40

		
		rick?.run(SKAction.sequence([SKAction.wait(forDuration: 2),SKAction.move(to: CGPoint(x: (rick?.position.x)!+4, y:(rick?.position.y)! + 10), duration: 0.5), SKAction.move(to: CGPoint(x: (rick?.position.x)!+45, y:(rick?.position.y)!-50), duration: 0.5), SKAction.wait(forDuration: 18),SKAction.setTexture(SKTexture(imageNamed: "rick_left")),SKAction.wait(forDuration: 16.7),SKAction.setTexture(SKTexture(imageNamed: "rick_surprised")),SKAction.wait(forDuration: 2.2),SKAction.repeatForever(SKAction.animate(with: rickFrames, timePerFrame: 0.15))]))
		
		
		rick?.run(SKAction.sequence([SKAction.wait(forDuration: 40.2),SKAction.moveTo(x: 275, duration: 2.5)]))
		
		morty?.run(SKAction.sequence([SKAction.wait(forDuration: 40.2),SKAction.setTexture(SKTexture(imageNamed: "morty_surprised")),SKAction.wait(forDuration: 2),SKAction.moveTo(x: 250-(rick?.size.width)!/1.8, duration: 0.75)]), completion: { () -> Void in self.startGame()})
		morty?.run(SKAction.sequence([SKAction.wait(forDuration: 42.2),SKAction.repeatForever(SKAction.animate(with: mortyFrames, timePerFrame: 0.15))]))
		
		
		
		
		
		
		
		morty?.run(SKAction.sequence([SKAction.wait(forDuration: 2),SKAction.move(to: CGPoint(x: (morty?.position.x)!-4, y:(morty?.position.y)! + 10), duration: 0.5), SKAction.move(to: CGPoint(x: (morty?.position.x)!-20, y:(morty?.position.y)!-65), duration: 0.5) , SKAction.wait(forDuration: 1), SKAction.setTexture(SKTexture(imageNamed: "morty_1")),SKAction.wait(forDuration: 0.5),SKAction.playSoundFileNamed("dialog.mp3", waitForCompletion: false)]))
		
		cam.run(SKAction.sequence([SKAction.wait(forDuration: 5),SKAction.scale(by: 0.6, duration: 1),SKAction.wait(forDuration: 32.7),SKAction.scale(by: 2.5, duration: 0.5)]))
		cam.run(SKAction.sequence([SKAction.wait(forDuration: 5),SKAction.move(to: (rick?.position)!, duration: 1),SKAction.wait(forDuration: 32.7),SKAction.move(to: CGPoint(x:frame.size.width/3.5,y:frame.size.height/2), duration: 0.5)]))
		
		
		
		let monster_texture = SKTexture(imageNamed: "monster.png")
		monster = SKSpriteNode(texture: monster_texture, color: SKColor.clear, size: CGSize(width: 330, height: 232))
		
		
		monster?.position = CGPoint(x: -1 * (self.frame.width), y:  0)
		self.addChild(monster!)
		monster?.run(SKAction.sequence([SKAction.wait(forDuration: 35.7),SKAction.move(to: CGPoint(x:-1 * (self.frame.width/2), y:0), duration: 2),SKAction.wait(forDuration: 3)]))
		
	
		
		
	}
	
	
	func startGame(){
		
		cutSceneOver = true;
		enemyInView = false;
		
		
		rick?.zPosition = 30
		let soundFilePath = Bundle.main.path(forResource: "8bitTheme", ofType: "mp3")
		let soundFileURL = NSURL(fileURLWithPath: soundFilePath!)
		do
		{
			audioPlayer = try AVAudioPlayer(contentsOf: soundFileURL as URL, fileTypeHint: nil)
		}
		catch let error as NSError
		{
			print(error.description)
		}
		audioPlayer?.numberOfLoops = -1 //infinite
		audioPlayer?.play()
		audioPlayer?.volume = 0.7
		
		rick?.physicsBody = SKPhysicsBody(texture: (rick?.texture)!, size: (rick?.size)!)
		
		rick?.physicsBody?.categoryBitMask = rickCategory
		rick?.physicsBody?.allowsRotation = false
	
		
		morty?.physicsBody = SKPhysicsBody(rectangleOf: (morty?.size)!)
		morty?.physicsBody?.categoryBitMask = rickCategory
		morty?.physicsBody?.allowsRotation = false
		

		let floor = childNode(withName: "floor")
		floor?.physicsBody = SKPhysicsBody(rectangleOf: (floor as! SKSpriteNode).size)
		floor?.physicsBody?.isDynamic = false
		
		let pinJoint = SKPhysicsJointFixed.joint(withBodyA: (morty?.physicsBody)!, bodyB: (rick?.physicsBody)!, anchor: (rick?.position)!)
		
		
		self.physicsWorld.add(pinJoint)
		
		spawnEnemies()
		
	
	}


	func shoot(){
		rick?.run(SKAction.repeat(SKAction.animate(with: gunFrames, timePerFrame: 0.15), count: 3), completion:{
			SKAction.repeatForever( SKAction.animate(with: self.rickFrames, timePerFrame: 0.15))
		})
		
		
		
		let portalTexture = SKTexture(imageNamed: "projectile")
		let portal = SKSpriteNode(texture: portalTexture, color: SKColor.clear, size: CGSize(width: 21, height: 7))
		portal.physicsBody = SKPhysicsBody(rectangleOf: portal.size)
		portal.physicsBody?.affectedByGravity = false
		portal.physicsBody?.collisionBitMask = 0
		portal.position = CGPoint(x: (rick?.position.x)! + (rick?.size.width)!/2, y: (rick?.position.y)!*1.42)
		portal.physicsBody?.categoryBitMask = portalCategory
		
		self.addChild(portal)
		if(enemyInView!){
			self.enemyInView = false

			portal.run(SKAction.sequence([SKAction.playSoundFileNamed("rickportal.wav", waitForCompletion: false), SKAction.move(to: CGPoint(x: (enemy?.position.x)! + (enemy?.size.width)!/10, y: (enemy?.position.y)!),duration: 2)]), completion:{
				portal.run(SKAction.scale(by: 6, duration: 0.5))
	
				
				
				
				let top_tex = SKTexture(imageNamed: "top_half")
				let topHalf = SKSpriteNode(texture: top_tex, color: SKColor.clear, size: top_tex.size())
				
				topHalf.position = CGPoint(x: (self.enemy?.position.x)!, y: (self.enemy?.position.y)! + (self.enemy?.size.height)!/4)
				topHalf.physicsBody = SKPhysicsBody(rectangleOf: topHalf.size)
				topHalf.physicsBody?.affectedByGravity = false
				self.addChild(topHalf)

				
				let blood1 = SKEmitterNode(fileNamed: "blood")
				blood1?.setScale(0.2)
				blood1?.position = CGPoint(x: (self.enemy?.position.x)!+20, y: (self.enemy?.position.y)!)
			//	blood1?.zRotation = -3.14159
				blood1?.physicsBody = SKPhysicsBody(circleOfRadius: 20)
				self.addChild(blood1!)
				
				
				
				
				let pinJoint1 = SKPhysicsJointFixed.joint(withBodyA: (blood1?.physicsBody)!, bodyB: (topHalf.physicsBody)!, anchor: (blood1?.position)!)
				
				
				self.physicsWorld.add(pinJoint1)
	
				
				
				
				
				
				let bottom_tex = SKTexture(imageNamed: "bottom_half")
				let bottomHalf = SKSpriteNode(texture: bottom_tex, color: SKColor.clear, size: bottom_tex.size())

				bottomHalf.position = CGPoint(x: (self.enemy?.position.x)!, y: (self.enemy?.position.y)! - (self.enemy?.size.height)!/4)
		
				
				
				let blood2 = SKEmitterNode(fileNamed: "blood")
				blood2?.setScale(0.2)
				blood2?.position = CGPoint(x: (self.enemy?.position.x)!+20, y: (self.enemy?.position.y)!)
				blood2?.zRotation = 3.14159
				blood2?.physicsBody = SKPhysicsBody(circleOfRadius: 20)

				bottomHalf.physicsBody = SKPhysicsBody(rectangleOf: bottomHalf.size)

				bottomHalf.physicsBody?.categoryBitMask = enemyCategory
				bottomHalf.physicsBody?.collisionBitMask = floorCategory
			
				topHalf.zPosition = 14
				blood1?.zPosition = 13
				portal.zPosition = 12
				
				blood2?.zPosition = 10
				bottomHalf.zPosition = 11

				
				self.addChild(blood2!)
			
					
				self.addChild(bottomHalf)
				self.enemy?.removeFromParent()

			let pinJoint2 = SKPhysicsJointFixed.joint(withBodyA: (blood2?.physicsBody)!, bodyB: (bottomHalf.physicsBody)!, anchor: (blood2?.position)!)
				
				self.physicsWorld.add(pinJoint2)


				topHalf.run(SKAction.sequence([SKAction.moveTo(y: topHalf.position.y + 200, duration: 0.7),SKAction.move(to: portal.position, duration: 0.7)]), completion:{
					portal.physicsBody?.isDynamic = false
					topHalf.physicsBody?.isDynamic = false
					bottomHalf.physicsBody?.isDynamic = false
					blood1?.removeFromParent()
					blood2?.physicsBody?.isDynamic = false


					topHalf.run(SKAction.scale(by: 0.1, duration: 1), completion:{
						
						portal.run(SKAction.scale(by: 0.1, duration: 0.5),completion:{
							portal.removeFromParent()
							bottomHalf.removeFromParent()
							blood2?.removeFromParent()
							topHalf.removeFromParent()
							blood1?.removeFromParent()
						})
					})
					
				})
			})
		}
		else{
			portal.run(SKAction.sequence([SKAction.playSoundFileNamed("rickportal.wav", waitForCompletion: false), SKAction.moveTo(x: 1000, duration: 4)]), completion:{
				portal.removeFromParent()
			})
		}
	}
	
	
	
	
	
	
	
	
	
	func spawnEnemies(){
		let delay = SKAction.wait(forDuration: 10, withRange: 3)
		
		let spawn = SKAction.run {
			if(!self.enemyInView!){
				let enemyTexture = SKTexture(imageNamed: "gromflomite")
				self.enemy = SKSpriteNode(texture: enemyTexture, color: SKColor.clear, size: CGSize(width: 108, height: 98))
				
				
				self.enemy?.position = CGPoint(x: 1000, y: (self.rick?.position.y)!)
				
				
				
				self.enemy?.zPosition = 1
				

				self.addChild(self.enemy!)
				
				
				self.enemy?.run(SKAction.moveTo(x: 700, duration: 2))
				
				

				
			
				
				
				self.enemyInView = true;
			}
		}
		
		let seq = SKAction.sequence([delay,spawn])
		self.run(SKAction.repeatForever(seq) , withKey: "spawning enemies")
	
	}


	func setUpTextures(){
		rickFrames = [SKTexture]()
		rickFrames.append(SKTexture(imageNamed: "rick_1"))
		rickFrames.append(SKTexture(imageNamed: "rick_2"))
		rickFrames.append(SKTexture(imageNamed: "rick_3"))
		rickFrames.append(SKTexture(imageNamed: "rick_4"))
		
		gunFrames = [SKTexture]()

		gunFrames.append(SKTexture(imageNamed: "rick_gun_1"))
		gunFrames.append(SKTexture(imageNamed: "rick_gun_2"))
		gunFrames.append(SKTexture(imageNamed: "rick_gun_3"))
		gunFrames.append(SKTexture(imageNamed: "rick_gun_4"))

		
		mortyFrames = [SKTexture]()
		mortyFrames.append(SKTexture(imageNamed: "morty_1"))
		mortyFrames.append(SKTexture(imageNamed: "morty_2"))
		mortyFrames.append(SKTexture(imageNamed: "morty_3"))
		mortyFrames.append(SKTexture(imageNamed: "morty_4"))
		
	}
}
