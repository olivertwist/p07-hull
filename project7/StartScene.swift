//
//  StartScene.swift
//  project7
//
//  Created by oliver on 4/24/17.
//  Copyright © 2017 oliver. All rights reserved.
//

import SpriteKit
import GameplayKit


class StartScene: SKScene{
	
	
	override func didMove(to view: SKView) {
	
		let portal = SKSpriteNode(texture: SKTexture(cgImage: #imageLiteral(resourceName: "portal00") as! CGImage))
		
		
		let textureAtlas = SKTextureAtlas(named: "portal")
		let frames = ["portal00", "portal01", "portal02", "portal03"].map { textureAtlas.textureNamed($0) }
		let animate = SKAction.animate(with: frames, timePerFrame: 0.2)
		let forever = SKAction.repeatForever(animate)
		portal.position = CGPoint(x: 0, y: 0)
		
		self.addChild(portal)
		
//		portal.run(forever)
		
	}
}
