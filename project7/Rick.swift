//
//  Rick.swift
//  project7
//
//  Created by oliver on 4/22/17.
//  Copyright © 2017 oliver. All rights reserved.
//

import Foundation
import SpriteKit


class Rick: SKSpriteNode {

	var health: Int?
	
	init(){
		let texture = SKTexture(imageNamed: "rick_start.png")
		super.init(texture: texture, color: SKColor.clear, size: CGSize(width: texture.size().width/2.0, height: texture.size().height/2.0))
		self.physicsBody = SKPhysicsBody(rectangleOf: self.size)
		self.physicsBody?.categoryBitMask = RickCategory
		self.physicsBody?.contactTestBitMask = EnemyCategory | FlaskCategory
		self.name = "rick"
	}
	
	func animate(){
		let textureAtlas = SKTextureAtlas(named: "rick")
		let frames = ["rick_1.png","rick_2.png", "rick_3.png", "rick_4.png"].map { textureAtlas.textureNamed($0) }
		let animate = SKAction.animate(with: frames, timePerFrame: 0.2)
		let forever = SKAction.repeatForever(animate)
		self.run(forever)
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
}
